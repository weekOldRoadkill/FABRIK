;; Setup
(love.graphics.setLineJoin :none)
(local font (love.graphics.newFont 18))

(local rope [])
(for [i 1 500] (tset rope i {:x 640 :y 360}))




;; Love's Scroll Wheel Function
(fn love.wheelmoved [x y] (if
		(> y 0) (for [_ 1 10] (tset rope (+ (# rope) 1) {:x (love.mouse.getX) :y (love.mouse.getY)}))
		(not= (# rope) 10) (for [_ 1 10] (tset rope (# rope) nil))))




;; Love's Update Function
(fn love.update [delta]

	(tset rope (# rope) :x (love.mouse.getX))
	(tset rope (# rope) :y (love.mouse.getY))

	(for [i (- (# rope) 1) 2 -1]
		(local a (math.atan2 (- (. rope i :y) (. rope (+ i 1) :y)) (- (. rope i :x) (. rope (+ i 1) :x))))
		(tset rope i :x (+ (math.cos a) (. rope (+ i 1) :x)))
		(tset rope i :y (+ (math.sin a) (. rope (+ i 1) :y))))

	(for [i 2 (# rope)]
		(local a (math.atan2 (- (. rope i :y) (. rope (- i 1) :y)) (- (. rope i :x) (. rope (- i 1) :x))))
		(tset rope i :x (+ (math.cos a) (. rope (- i 1) :x)))
		(tset rope i :y (+ (math.sin a) (. rope (- i 1) :y)))))




;; Love's Draw Function
(fn love.draw []

	(local rope* [])
	(for [i 1 (# rope)]
		(tset rope* (+ (# rope*) 1) (. rope i :x))
		(tset rope* (+ (# rope*) 1) (. rope i :y)))
	(love.graphics.line rope*)

	(love.graphics.print "Scroll to change the length of the rope." font))
