;; Config
(fn love.conf [t]
	(set t.version 11.4)
	(set t.window.msaa 8)
	(set t.window.vsync 0)
	(set t.window.height 720)
	(set t.window.width 1280)
	(set t.window.title :FABRIK))
