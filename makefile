out = FABRIK


build: $(out).love


run: build
	love $(out).love


$(out).love: conf.lua main.lua
	zip -r $(out).love conf.lua main.lua 
	rm -fr conf.lua main.lua


conf.lua:
	fennel -c --use-bit-lib --lua luajit src/conf.fnl | tee conf.lua
	luajit -b conf.lua conf.lua


main.lua:
	fennel -c --use-bit-lib --lua luajit src/main.fnl | tee main.lua
	luajit -b main.lua main.lua
