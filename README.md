# FABRIK
*A small demonstration in Fennel*


## Building

This demonstration was made with Fennel and LuaJIT so you will need both installed. LuaJIT is most likely
available through your system's package manager. Fennel is most likely not available through your system's
package manager so installation through LuaRocks is recommended.

Once all the dependencies are installed, run `make` in the directory with the makefile to build. Once built,
you should be left with a file called `FABRIK.love`, which is all LÖVE will need to run the demonstration.


## Running

This demonstration uses the LÖVE framework at runtime so it will need to be installed as well. LÖVE is most
likely available through your system's package manager, but if not it is also available through LuaRocks.

Once all the runtime dependencies are installed, you can run the demonstration by running `love FABRIK.love`
in the directory with the file.


## Notes

-	If you're trying to build or even run this on Windows, seek medical attention immediately.
-	This is my first time using Fennel, I am very experienced with Lua but it is my very first time using a
	lispy language.
